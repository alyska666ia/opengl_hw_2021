//
// Created by Анна Исаева on 28.02.2021.
//
#include "PerlinNoise_2D.cpp"
#include <vector>
#include <SOIL2.h>
#include <iostream>
using namespace std;

class HeightMap {
public:
    int height;
    int width;
    vector<vector<float>> heightMap;
    float max = 0;

    void generateFromImage(const char* path) {
        heightMap.clear();
        int channels;
        unsigned char* ht_map = SOIL_load_image(path, &width, &height, &channels, SOIL_LOAD_L);
        for (int i = 0; i < height; i++) {
            heightMap.emplace_back(width);
            for (int j = 0; j < width; j++) {
                heightMap[i][j] = ht_map[width * i + j];
                if (max < heightMap[i][j]) {
                    max = heightMap[i][j];
                }
            }
        }
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                heightMap[i][j] /= max;
            }
        }
    }

    void generate(int height, int width) {
        this->height = height;
        this->width = width;
        heightMap.clear();
        for (int i = 0; i < height; i++) {
            heightMap.emplace_back(width);
            for (int j = 0; j < width; j++) {
                heightMap[i][j] = float(perlinNoise_2D(double(i), double(j)));
            }
        }
    }
};