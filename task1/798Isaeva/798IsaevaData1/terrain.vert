#version 330

//стандартные матрицы для преобразования координат
uniform mat4 modelMatrix; //из локальной в мировую
uniform mat4 viewMatrix; //из мировой в систему координат камеры
uniform mat4 projectionMatrix; //из системы координат камеры в усеченные координаты

layout(location = 0) in vec3 vertexPosition; //координаты вершины в локальной системе координат
layout(location = 1) in vec3 vertexNormal; //нормаль в локальной системе координат

out vec4 color;

void main() {
    float H = vertexPosition.z;
    // преобразуем высоту(float в промежутке [0;1]) в цвет
    float R = 2.0f - abs(H * 6.0f - 4.0f);
    float G = 2.0f - abs(H * 6.0f - 2.0f);
    float B = abs(H * 6.0f - 3.0f) - 1.0f;

    color.rgb = vec3(max(0.0, min(1.0, R)), max(0.0, min(1.0, G)), max(0.0, min(1.0, B)));
    color.a = 1;
    // устанавливаем положение точки
    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
}
