//
// Created by Анна Исаева on 28.02.2021.
//
#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include "HeightMap.cpp"

#include <glm/gtx/transform.hpp>


class TerrainApplication : public Application {
public:
    TerrainApplication();

    void makeScene() override;
    void draw() override;

private:
    MeshPtr surface;
    ShaderProgramPtr shader;
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;

    void makeVerticesAndNorms(HeightMap& terrain);
    void makeSurface();
    void makeScaling(float scale);
};


void TerrainApplication::makeVerticesAndNorms(HeightMap& terrain) {
    for (int i = 0; i < terrain.height - 1; i++) {
        for (int j = 0; j < terrain.width - 1; j++) {
            // первый треугольник
            glm::vec3 x(float(i)/float(terrain.height - 1), float(j)/float(terrain.width - 1), terrain.heightMap[i][j]);
            glm::vec3 y(float(i)/float(terrain.height - 1), float(j + 1)/float(terrain.width - 1), terrain.heightMap[i][j + 1]);
            glm::vec3 z(float(i + 1)/float(terrain.height - 1), float(j)/float(terrain.width - 1), terrain.heightMap[i + 1][j]);
            vertices.emplace_back(x);
            vertices.emplace_back(y);
            vertices.emplace_back(z);
            normals.emplace_back(glm::normalize(x));
            normals.emplace_back(glm::normalize(y));
            normals.emplace_back(glm::normalize(z));

            // второй треугольник
            x = glm::vec3(float(i)/float(terrain.height - 1), float(j + 1)/float(terrain.width - 1), terrain.heightMap[i][j + 1]);
            y = glm::vec3(float(i + 1)/float(terrain.height - 1), float(j)/float(terrain.width - 1), terrain.heightMap[i + 1][j]);
            z = glm::vec3(float(i + 1)/float(terrain.height - 1), float(j + 1)/float(terrain.width - 1), terrain.heightMap[i + 1][j + 1]);
            vertices.emplace_back(x);
            vertices.emplace_back(y);
            vertices.emplace_back(z);
            normals.emplace_back(glm::normalize(x));
            normals.emplace_back(glm::normalize(y));
            normals.emplace_back(glm::normalize(z));
        }
    }
}

TerrainApplication::TerrainApplication() {
    HeightMap terrain;
    // terrain.generate(1024, 1024);
    terrain.generateFromImage("798IsaevaData1/images/img_4.png");
    makeVerticesAndNorms(terrain);
}

void TerrainApplication::makeSurface() {
    DataBufferPtr dataBufferVertices = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    DataBufferPtr dataBufferNormals = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    dataBufferVertices->setData(vertices.size() * sizeof(float) * 3, vertices.data());
    dataBufferNormals->setData(normals.size() * sizeof(float) * 3, normals.data());

    surface = std::make_shared<Mesh>();
    surface->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, dataBufferVertices);
    surface->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, dataBufferNormals);
    surface->setPrimitiveType(GL_TRIANGLES);
    surface->setVertexCount(vertices.size());
}


void TerrainApplication::makeScene() {
    Application::makeScene();
    //Создание и загрузка мешей
    makeSurface();
    makeScaling(10);
    //Инициализация шейдера
    shader = std::make_shared<ShaderProgram>("798IsaevaData1/terrain.vert", "798IsaevaData1/terrain.frag");
    _cameraMover = std::make_shared<FreeCameraMover>();
}

void TerrainApplication::draw() {
    Application::draw();
    //Получаем размеры экрана (окна)
    int width, height;
    glfwGetFramebufferSize(_window, &width, &height);
    //Устанавливаем порт вывода на весь экран (окно)
    glViewport(0, 0, width, height);

    //Очищаем порт вывода (буфер цвета и буфер глубины)
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //Подключаем шейдер
    shader->use();
    //Загружаем на видеокарту значения юниформ-переменных
    shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
    shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

    //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
    shader->setMat4Uniform("modelMatrix", surface->modelMatrix());
    surface->draw();

}

void TerrainApplication::makeScaling(float scale) {
    glm::tmat4x4<double> modelMatrix = glm::scale(glm::vec3(1.f * scale, 1.f * scale, 0.5));
    surface->setModelMatrix(modelMatrix);
}

int main() {
    TerrainApplication application;
    application.start();
    return 0;
}